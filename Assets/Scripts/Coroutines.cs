﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coroutines : MonoBehaviour {

	//executar comando, esperar tempo randomico, executar comando novamente

	void Start () {

		StartCoroutine (Testando ());
	}

	IEnumerator Testando () {

		while (true) {// declarar tempo randomico
			float wait_time = Random.Range (0, 2);
			//mostrar executando a cada wait_time

			Debug.Log ("Gonna sleep for: " + wait_time + " seconds :)");
			yield return new WaitForSeconds (wait_time); //esperar por tempo randomico de 0 a 20
		}
	}
}

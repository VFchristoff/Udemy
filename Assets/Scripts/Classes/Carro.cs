﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class Carro {

	public enum TipoDeCombustivel {Gasolina = 0, Diesel = 1, Alcool = 2}

	//reune varias variaveis dentro da classe
	public string Fabricante;
	public string Modelo;
	public int Ano;
	public int Potencia;
	public int VelocidadeMaxima;
	private string NumeroDeSerie;
	public bool Poluente;
	public TipoDeCombustivel Combustivel;
	public static int NumeroDeRodas = 4;

	public Carro (string fabricante, string modelo, int ano, 
		int potencia, int velocidadeMaxima, TipoDeCombustivel combustivel) {
		//parametros
		Fabricante = fabricante;
		Modelo = modelo;
		Ano = ano;
		Potencia = potencia;
		VelocidadeMaxima = velocidadeMaxima;
		Combustivel = combustivel;
		SetarPoluenteSwitch ();
	}

	public Carro (string fabricante, string modelo, int ano, 
		int potencia, int velocidadeMaxima, bool setarNumeroDeSerie, TipoDeCombustivel combustivel) {
		//parametros
		Fabricante = fabricante;
		Modelo = modelo;
		Ano = ano;
		Potencia = potencia;
		VelocidadeMaxima = velocidadeMaxima;
		Combustivel = combustivel;
		SetarPoluenteSwitch ();
		if (setarNumeroDeSerie) {
			SetarNumeroDeSerie ();
		}
	}

	private void SetarPoluenteIF () {
		if (Combustivel == TipoDeCombustivel.Alcool) {
			Poluente = false;
		} 
		else if (Combustivel == TipoDeCombustivel.Diesel || Combustivel == TipoDeCombustivel.Gasolina) {
			Poluente = true;
		}
	}
	private void SetarPoluenteSwitch () {
		switch (Combustivel) {
		case TipoDeCombustivel.Alcool:
			Poluente = false;
			break;
		case TipoDeCombustivel.Diesel: case TipoDeCombustivel.Gasolina:
			Poluente = true;
			break;
		}
	}

	public void SetarNumeroDeSerie () {
		NumeroDeSerie = Ano.ToString () + Potencia.ToString () + VelocidadeMaxima.ToString ();
	}
	//retorna o valor do numero de serie
	public string LerNumeroDeSerie () {
		return NumeroDeSerie;
	}

	//para definir qual carro sera mais potente:
	public bool MaisPotenteQue (Carro carroComparado) {
		return Potencia > carroComparado.Potencia;
		/* sempre que eu acessar a potencia,
		 * se refere a variavel da instancia que ta chamando essa funcao
		*/
	}

}
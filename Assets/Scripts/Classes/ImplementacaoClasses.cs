﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImplementacaoClasses : MonoBehaviour {

	public Carro carroA;
	public Carro carroB;

	// Use this for initialization
	void Start () {
		//alocar espaço na memoria é opcional quando a classe é serialized
		//alocando espaço na memória para uma variável do tipo Carro

		//carroA informacoes
		carroA = new Carro ("Ford","Camaro", 2018, 501, 400, Carro.TipoDeCombustivel.Alcool);
		carroA.SetarNumeroDeSerie ();
		print ("Carro A = " + carroA.LerNumeroDeSerie ());
		//carroB informacoes
		carroB = new Carro ("Chevrolet","Fiesta",2018, 500, 300, Carro.TipoDeCombustivel.Diesel);
		carroB.SetarNumeroDeSerie ();
		print ("Carro B = " + carroB.LerNumeroDeSerie ());
		print ("Carro A mais potente que o Carro B?" + carroA.MaisPotenteQue (carroB));
		//resposta verdadeira nessa condicao e nesses valores
		print ("Um carro possui " + Carro.NumeroDeRodas + " rodas.");

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
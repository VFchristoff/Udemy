﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Human : Mamifero {

	public string Name;
	public string Gender;
	public string Job;

	public Human (string job, string name, string gender, string emprego, Color colorOfTheFurr, int age) : base ( colorOfTheFurr, "homo sapiens sapiens", age) {
		//it's putting homo sapiens sapiens string for every human, pre defined species, so you can remove the parametre at ImplementacaoHerancas
		//when using the constructor, you can pick the class after the base class because
		//it contains all the base and the picked class, chain reaction (reacao em cadeia)
		Name = name;
		Gender = gender;
		Job = job;
	}

	public void Working () {
		if (Age > 18) {
			MonoBehaviour.print (Name + "is working");
		}
		else {
			MonoBehaviour.print (Name + "is a minor");
		}
	}

	public void Studying () {
		MonoBehaviour.print (Name + "is studying");
	}
}
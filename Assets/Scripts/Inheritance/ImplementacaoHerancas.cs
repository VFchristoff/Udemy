﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImplementacaoHerancas : MonoBehaviour {

	public Human human;
	public Mamifero elefante;

	void Start () {
		
		human = new Human ("Estudante", "John", "Male", "None", Color.blue, 17);
		elefante = new Mamifero (Color.cyan, "elefantus", 800);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using UnityEngine;

[System.Serializable]
//Mamifero is a subgenrer of Animal, in consequence Mamifero is inheriting of Animal
public class Mamifero : Animal {
	//this class is having access to any element public or protected of the class Animal
	public Color ColorOfTheFurr;

	public Mamifero (Color colorOfTheFurr, string species, int age) : base (species, age) {
		ColorOfTheFurr = colorOfTheFurr;
	}

	public void Existing () {
		MonoBehaviour.print ("The mamifero is existing");
	}
}
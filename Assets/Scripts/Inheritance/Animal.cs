﻿using System.Collections;
using UnityEngine;

//base class
public class Animal {

	public string Species;
	public int Age;

	public Animal (string species, int age) {
		Species = species;
		Age = age;
	}

	//parametres of the base class
	public void Motion () {
		MonoBehaviour.print ("The animal is moving");
	}
	public void Eat () {
		MonoBehaviour.print ("The animal is eating");
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : Mamifero {

	public string Name;

	public Dog (string name, Color colorOfTheFurr, int age) : base (colorOfTheFurr, "Canis", age) {
		Name = name;
	}

	public void Latir () {
		
	}
}

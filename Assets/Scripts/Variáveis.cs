﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variáveis : MonoBehaviour {

	// can declare variables
	// tipos basicos: int, float, string, bool

	// tipo de acesso (opcional) + tipo da variavel (obrigatorio) + nome da variavel (obrigatorio) + = valor (opcional)
	public int myInteger;
	public float myFloat;
	public string myText;
	public bool myBoolean;
	public int velocidadeDoMeuPersonagem;

	void Start () {
		// can declare variables
		
	}
	
	// Update is called once per frame
	void Update () {
		// can declare variables	
	}
}

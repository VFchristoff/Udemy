﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionsII : MonoBehaviour {

	public int[] valores;
	public float valorQualquer;

	void Start () {

		TrocarValorDeIndice (valores, 0, -3);
		SomarComTres (ref valorQualquer);
		
	}

	void TrocarValorDeIndice (int[] vetor, int indice, int valor) {
		
		vetor[indice] = valor;
	}

	void SomarComTres (ref float numero) {
		numero = 2f;
		numero = numero + 3f;
	}

	//int Dividir (float valor, float divisor, ref float resto);


}

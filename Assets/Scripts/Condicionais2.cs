﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condicionais2 : MonoBehaviour {


	public int idade;
	public bool podeBeber, idadeInvalida;

	void Start () {
		if (idade >= 18) {
			//executar o que esta aqui sempre que a idade for maior ou igual a 18
			podeBeber = true;

			idadeInvalida = false;

		} else if (idade < 0) {
			//executar sempre que a condicao principal nao for aceita
			podeBeber = false;
			idadeInvalida = true;
		} else {
			podeBeber = false;
			idadeInvalida = false;
		}


	}
	

	void Update () {
		
	}
}
